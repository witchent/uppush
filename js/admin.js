function setKeepalive() {
    let keepalive = document.getElementById("keepalive").value
    if (keepalive.length > 0) {
        let xhr = new XMLHttpRequest()
        xhr.onreadystatechange = function() {
            if (this.readyState == 4 && this.status == 200) {
                alert("Keepalive changed to " + keepalive)
            }
        }
        let data = {}
        data.keepalive = keepalive
        let json = JSON.stringify(data)
        xhr.open("PUT", "../../apps/uppush/keepalive/", true)
        xhr.setRequestHeader('Content-type','application/json charset=utf-8')
        xhr.send(json)
    }
}

document.addEventListener('DOMContentLoaded', function() {
    const addListener = () => {
        document.querySelector("#setKeepaliveBtn").addEventListener("click", setKeepalive)
    }
    if (document.querySelector("#setKeepaliveBtn")) {
        addListener()
    } else {
        window._nc_event_bus.subscribe('core:user-menu:mounted', addListener)
    }

})
